//
//  UIViewControllerExtensions.swift
//  Transight
//
//  Created by Anumol Andrews on 13/08/19.
//  Copyright © 2019 Anumol Andrews. All rights reserved.
//

import UIKit
import MKProgress

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
//Toast message implementation
extension UIViewController {
    
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: 0, y: self.view.frame.size.height-60, width: self.view.frame.size.width, height: 60))
        toastLabel.backgroundColor = UIColor.blue.withAlphaComponent(1.0)
        toastLabel.font = UIFont(name: "Apple SD Gothic Neo", size: 16.0)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        //toastLabel.layer.cornerRadius = 10;
        //toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 6.0, delay: 2.0, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    } }
extension UIViewController{
    func progressbarshow()
    {
        MKProgress.config.hudType = .radial
        //        MKProgress.config.width = 50.0
        //        MKProgress.config.height = 50.0
        MKProgress.config.hudColor = .white
        MKProgress.config.backgroundColor = UIColor(white: 0, alpha: 0.55)
        MKProgress.config.cornerRadius = 16.0
        MKProgress.config.fadeInAnimationDuration = 0.2
        MKProgress.config.fadeOutAnimationDuration = 0.25
        MKProgress.config.hudYOffset = 15
        
        MKProgress.config.circleRadius = 40.0
        MKProgress.config.circleBorderWidth = 1.0
        MKProgress.config.circleBorderColor = .darkGray
        MKProgress.config.circleAnimationDuration = 0.9
        MKProgress.config.circleArcPercentage = 0.85
        MKProgress.config.logoImage  = UIImage(named:"splashBg-1")
        MKProgress.config.logoImageSize = CGSize(width: 40, height: 40)
        
        //MKProgress.config.activityIndicatorStyle = .whiteLarge
        //MKProgress.config.activityIndicatorColor = .black
        //MKProgress.config.preferredStatusBarStyle = .lightContent
        //MKProgress.config.prefersStatusBarHidden = false
        MKProgress.show()
    }
    
}
