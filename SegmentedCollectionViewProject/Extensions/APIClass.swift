//
//  APIClass.swift
//  SegmentedCollectionViewProject
//
//  Created by Anumol Andrews on 02/09/19.
//  Copyright © 2019 Anumol Andrews. All rights reserved.
//

import Foundation
struct ApiPaths {
    
    static let  headers = [
        "accessToken": "74c14fa62349c91c67607d8382656c431eb8e0b6-084e0343a0486ff05530df6c705c8bb4",
        "lat": "12.9716",
        "lng": "77.5946"
        
        ]
    
    static let baseUrlProd = "https://paygyft.com"
    static let categorlUrl = "/api/v1/user/store/search/popular"
    static let storeUrl = "/api/v1/user/store/search/"
    
    // static let appversion = "forget/"
}
