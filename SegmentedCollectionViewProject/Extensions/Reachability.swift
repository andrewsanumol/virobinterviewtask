//
//  Reachability.swift
//  virobproject
//
//  Created by Anumol Andrews on 30/08/19.
//  Copyright © 2019 Anumol Andrews. All rights reserved.
//

import Foundation
import SystemConfiguration
import Alamofire
public class Reachability {
    
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
}
