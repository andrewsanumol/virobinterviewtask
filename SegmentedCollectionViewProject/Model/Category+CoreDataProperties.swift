//
//  Category+CoreDataProperties.swift
//  
//
//  Created by Anumol Andrews on 02/09/19.
//
//

import Foundation
import CoreData


extension Category {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Category> {
        return NSFetchRequest<Category>(entityName: "Category")
    }

    @NSManaged public var categoryName: String
    @NSManaged public var categoryId: String
    @NSManaged public var stores: NSSet

}

// MARK: Generated accessors for owner
extension Category {

    @objc(addOwnerObject:)
    @NSManaged public func addToOwner(_ value: Store)

    @objc(removeOwnerObject:)
    @NSManaged public func removeFromOwner(_ value: Store)

    @objc(addOwner:)
    @NSManaged public func addToOwner(_ values: NSSet)

    @objc(removeOwner:)
    @NSManaged public func removeFromOwner(_ values: NSSet)

}
extension Board {
    func addListObject(value:Store) {
        var items = self.mutableSetValueForKey("stores");
        items.addObject(value)
    }
    
    func removeListObject(value:Store) {
        var items = self.mutableSetValueForKey("stores");
        items.removeObject(value)
    }
}
