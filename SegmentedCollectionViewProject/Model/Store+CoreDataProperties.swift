//
//  Store+CoreDataProperties.swift
//  
//
//  Created by Anumol Andrews on 02/09/19.
//
//

import Foundation
import CoreData


extension Store {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Store> {
        return NSFetchRequest<Store>(entityName: "Store")
    }

    @NSManaged public var storeLogo: String
    @NSManaged public var name: String
    @NSManaged public var storeOffer: String
    @NSManaged public var category: Category

}
