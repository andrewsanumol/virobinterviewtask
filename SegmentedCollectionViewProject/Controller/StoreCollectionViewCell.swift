//
//  StoreCollectionViewCell.swift
//  virobproject
//
//  Created by Anumol Andrews on 01/09/19.
//  Copyright © 2019 Anumol Andrews. All rights reserved.
//

import UIKit

class StoreCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var storeName: UILabel!
    
    @IBOutlet weak var offerLabel: UILabel!
    @IBOutlet weak var storeImage: UIImageView!
}
