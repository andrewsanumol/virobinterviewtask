    //
    //  MainVC.swift
    //  SegmentedCollectionViewProject
    //
    //  Created by Anumol Andrews on 02/09/19.
    //  Copyright © 2019 Anumol Andrews. All rights reserved.
    //

    import CoreLocation
    import UIKit
    import Alamofire
    import SwiftyJSON
    import MKProgress
    import CoreData

    class MainVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {


    //LET CONSTANTS
    let myColor : UIColor = UIColor.blue

    //VARIABLES
    var storeentities : [NSManagedObject] = []
    var categoriesresponse = JSON()
    var categoryheaderArray : [String] = []
    var categoryentities : [NSManagedObject] = []
    var categorySelection = String()

    //IB OUTLETS
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var storecollectionview: UICollectionView!
    @IBOutlet weak var scrollcontentview: UIView!


    //VIEWDIDLOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        getcategoryAPICall()
    }

    //VIEWDIDLAYOUTSUBVIEWS
    override func viewDidLayoutSubviews() {
        self.scrollview.isScrollEnabled = true
        self.scrollview.contentSize = scrollcontentview.frame.size
    }

    //SEGMENTCONTROL
    func segmentWithTitles(titles : [String])  {
        let segmentcontrol = UISegmentedControl(items : titles)
        segmentcontrol.center = self.scrollcontentview.center
        segmentcontrol.selectedSegmentIndex = 0
        segmentcontrol.addTarget(self, action: #selector(MainVC.indexChanged(_:)), for: .valueChanged)
        segmentcontrol.clipsToBounds = true
        segmentcontrol.layer.cornerRadius = 5.0
        segmentcontrol.backgroundColor = .blue
        segmentcontrol.tintColor = .white
        self.scrollcontentview.addSubview(segmentcontrol)
        
    }

    //SEGMENT SELECTION
    @objc func indexChanged(_ sender: UISegmentedControl) {
        let tag = sender.selectedSegmentIndex
        let singlecategory = categoryentities[tag]
        let categorySelection = singlecategory.value(forKeyPath: "categoryId") as! String
        getstoreapiCall(param: categorySelection)
        self.storecollectionview.reloadData()
    }

    //COLLECTIONVIEW
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return storeentities.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = storecollectionview.dequeueReusableCell(withReuseIdentifier: "storecell", for: indexPath) as! StoreCollectionViewCell
        let singlestore = storeentities[indexPath.row]
        cell.storeName.text = singlestore.value(forKeyPath: "name") as? String
        let storeImage = singlestore.value(forKeyPath: "storeLogo") as? String
        cell.storeName.layer.cornerRadius = 10.0
        cell.storeImage.layer.cornerRadius = 10.0
        cell.offerLabel.layer.cornerRadius = 10.0
        cell.layer.cornerRadius = 10.0
        cell.layer.borderWidth = 1
        cell.layer.borderColor = myColor.cgColor
        let url = URL(string: storeImage!)
        
        DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async {
                    cell.storeImage.image = UIImage(data: data!)
            }
        }
        cell.offerLabel.text = singlestore.value(forKeyPath: "storeOffer") as? String
        return cell
        
    }
    //API CALLS(category data)
    func getcategoryAPICall(){
        
        //        if Reachability.isConnectedToInternet() {
        //            print("Yes! internet is available.")
        //            // do some tasks..
        let apiurl = ApiPaths.baseUrlProd+ApiPaths.categorlUrl
        progressbarshow()
        Alamofire.request(apiurl,method: .post,headers: ApiPaths.headers).responseJSON{ response in
                guard response.result.isSuccess
                    else {
                        print("Error while fetching data: \(String(describing: response.result.error))")
                        MKProgress.hide()
                        self.showToast(message: "Network Error!Please try later")
                        return
                }
                print(response.result.value!)
                let resultJSON : JSON = JSON(response.result.value!)
                self.getcategoryAPIjsonparsing(json: resultJSON)
                MKProgress.hide()
            }
        //        }
        //        else {
        //            print("Internet connection FAILED")
        //            showToast(message: " Your device is not connected to the internet")
        //        }
        
    }

    func getcategoryAPIjsonparsing(json: JSON){
        print("json parsing")
        
        //         categoriesresponse = JSON(json["categories"])
        //        if categoriesresponse.count > 0{
        //            for item in 0..<self.categoriesresponse.count{
        //                let singlecategory = category(categoryjson: self.categoriesresponse[item])
        //                categories.append(singlecategory)
        //
        //            }
        
        let categoryresult = json["categories"]
            if !categoryresult.isEmpty{
            for item in 0 ..< categoryresult.count{
                categorycreatedata(response: categoryresult[item])
                }
                categoryretreivedata()
                for title in categoryentities{
                    let singleheader = title.value(forKeyPath: "categoryName") as! String
                    categoryheaderArray.append(singleheader)
                }
            segmentWithTitles(titles: categoryheaderArray)
            MKProgress.hide()
        }
    }
    //API CALLS(store data)
        
    func getstoreapiCall(param: String){
//        if Reachability.isConnectedToInternet() {
//            print("Yes! internet is available.")
//            // do some tasks..
        let apiurl = ApiPaths.baseUrlProd + ApiPaths.storeUrl + "\(param)"
            progressbarshow()
            Alamofire.request(apiurl,method: .post,headers: ApiPaths.headers).responseJSON{ response in
                guard response.result.isSuccess
                    else {
                        print("Error while fetching data: \(String(describing: response.result.error))")
                        MKProgress.hide()
                        self.showToast(message: "Network Error!Please try later")
                        
                        return
                }
                print(response.result.value!)
                let resultJSON : JSON = JSON(response.result.value!)
                self.getstoreapijsonparsing(json: resultJSON)
                
            }
//        }
//        else {
//            print("Internet connection FAILED")
//            showToast(message: " Your device is not connected to the internet")
//        }
        
    }

    func getstoreapijsonparsing(json: JSON){
        print("json parsing")
        
        let storeresponse = json["data"]

        if !storeresponse.isEmpty{
            for item in 0..<storeresponse.count{
                storecreatedata(response: storeresponse[item])
            }
        
            storeretreivedata()
            self.storecollectionview.dataSource = self
            self.storecollectionview.delegate = self
            self.storecollectionview.reloadData()
            //storeentities.removeAll()
            MKProgress.hide()

        }else{
            MKProgress.hide()
            showToast(message: "No Stores To Display")
        }
    }

    //CORE DATA  METHODS(create and save)
    func storecreatedata(response:JSON){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Store", in: context)
        let singlestore = NSManagedObject(entity: entity!, insertInto: context)
        singlestore.setValue(response["store_name"].stringValue, forKeyPath: "name")
        singlestore.setValue(response["logo"].stringValue, forKeyPath:"storeLogo" )
        singlestore.setValue(response["offer"].stringValue, forKeyPath:"storeOffer" )
          do {
            try context.save()
            //storeentities.append(singlestore)
        } catch {
            print("Failed saving")
        }
        
    }
        
    func categorycreatedata(response:JSON){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Category", in: context)
        let singlestore = NSManagedObject(entity: entity!, insertInto: context)
        singlestore.setValue(response["category_slug"].stringValue, forKeyPath: "categoryId")
        singlestore.setValue(response["bcategory_name"].stringValue, forKeyPath:"categoryName" )
        
        do {
            try context.save()
            //storeentities.append(singlestore)
        } catch {
            print("Failed saving")
        }
    }
        
    //CORE DATA METHODS(fetch and update)

    func categoryretreivedata(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSManagedObject>(entityName: "Category")
        
    //        request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            if result.count > 0{
                for item in result {
                    categoryentities.append(item)
                }
            }
        } catch {
            
            print("Failed")
        }
    }
        func storeretreivedata(){
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let request = NSFetchRequest<NSManagedObject>(entityName: "Store")
            //request.predicate = NSPredicate(format: "Category.categoryId == %@ categorySelection")
            request.returnsObjectsAsFaults = false
            do {
                let result = try context.fetch(request)
                if result.count > 0{
                    for item in result {
                        storeentities.append(item)
                    }
                }
            } catch {
                
                print("Failed")
            }
        }
        
    }

